const EventEmitter = require('events');
const uuid = require('uuid');

class VotingLogger extends EventEmitter{
    /**
     * Logs an action of a player
     * @param {Number} subject PlayerId of subject
     * @param {String} action describes action
     * @param {Object} object describes object, format depends on action
     */
    logAction(subject, action, object){
        this.emit('action', {id: uuid.v4(), date: Date.now(), exApiMsg: {subject: subject, action: action, object: object}});
    }

    /**
     * Logs actions which are distributed
     * @param {Number} sender PlayerId of subject
     * @param {String} action describes distributed action
     */
    logDistributedAction(sender, action){
        this.emit('distributedAction', {id: uuid.v4(), date: Date.now(), exApiMsg: {sender: sender, action: action}});
    }

    /**
     * Logs messages
     * @param {String} msg Message to print to console
     */
    log(msg){
        this.emit("message", {id: uuid.v4(), date: Date.now(), msg});
    }
}

module.exports = VotingLogger;