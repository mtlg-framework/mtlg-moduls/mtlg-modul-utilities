// modified from https://stackoverflow.com/questions/3969475/javascript-pause-settimeout
/**
 * @class Timer
 * @memberof MTLG.utils
 * @param {number} delay The overall time until the timer triggers in milliseconds
 * @param {function} callback The function that is called when time is up
 * @example
 * //Create new Timer that triggers after 30 seconds
 * var myTimer = new MTLG.utils.timer(30*1000, function(){console.log("Timer Finished!");});
 * //Start timer
 * myTimer.start();
 * //Pause timer
 * myTimer.pause();
 * //Resume
 * myTimer.resume();
 * //Stop
 * myTimer.stop();
 * //Set some new values
 * myTimer.set(10*1000, function(){console.log("Timer 2 Finished!");});
 * //The code above will not trigger a single callback, first is stopped second not started
 */
function Timer(delay, callback) {
  var timerId, // Timeout ID
    start, // Start time
    remaining = delay; // Time Remaining (Delay is time to complete)

  // Set new values for delay and callback, reset remaining (does not reset timer)
  this.set = function(_delay, _callback){
    remaining = _delay;
    delay = _delay;
    callback = _callback;
  };

  // Returns true if time is up (based on remaining and start)
  this.timeUp = function(){
    return (Date.now() - start >= remaining);
  };

  // Pause timer (recalculates remaining)
  this.pause = function() {
    window.clearTimeout(timerId);
    remaining -= new Date() - start;
  };

  // Resume timer (set start + timeout)
  this.resume = function() {
    start = new Date();
    window.clearTimeout(timerId);
    timerId = window.setTimeout(callback, remaining);
  };

  // Start new timer (set start + timeout)
  this.start = function() {
    start = new Date();
    timerId = window.setTimeout(callback, remaining);
  };

  // Stop timer (reset remaining + clear timeout)
  this.stop = function() {
    window.clearTimeout(timerId);
    remaining = delay;
  }
}

module.exports = Timer;
